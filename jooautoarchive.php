<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.jooautoarchive
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (C) 2013-2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 *
 * @version     16.5.15
 */
defined('_JEXEC') or exit;

jimport('joomla.plugin.plugin');

/**
 * Class plgSystemJooautoarchive
 *
 * @since 1.5
 */
class plgSystemJooautoarchive extends JPlugin
{
    /**
     * @inheritdoc
     *
     * @since 1.5
     */
    public function onAfterInitialise ()
    {
        /** @var JDatabaseDriver $JDbo */
        $JDbo = JFactory::getDbo();

        /** @var JDate $JDate */
        $JDate = JFactory::getDate();

        /** @var Joomla\Registry\Registry $params */
        $params = $this->params;

        $action = $params->get('chaction', 0);
        $expire = $params->get('expire', 1);
        $more_times = $params->get('more_time', 30);
        $all_categories = $params->get('all_categories', 1);
        $categories = $params->get('categories', array());
        $nullDate = $JDbo->quote($JDbo->getNullDate());
        $nowDate = $JDbo->quote($JDate->toSql());

        $set = array();

        /** @var JDatabaseQuery $update */
        $update = $JDbo->getQuery(true)
            ->update('#__content');

        $set[] = "state = {$action}";
        if ($action != '2') $more_times = 0;
        elseif (($action == '2') && ($more_times < 1)) $more_times = 30;

        if ($more_times > 0) $set[] = "publish_down = DATE_ADD(publish_down, INTERVAL {$more_times} DAY)";
        if (($action == '2') && ($expire == '0')) $set[] = "publish_down = {$nullDate}";
        $update->set($set);

        if (!$all_categories && count($categories)) $update->where('catid IN (' . implode(', ', $categories) . ')');
        $update->where(array
        (
            'state = 1',
            "publish_down <= {$nowDate}",
            "publish_down != {$nullDate}",
        ));
        $JDbo->setQuery($update)->execute();

        $update = $JDbo->getQuery(true)
            ->update('#__content')
            ->set('state = 0')
            ->where(array
            (
                'state = 2',
                "publish_down <= {$nowDate}",
                "publish_down != {$nullDate}",
            ));
        $JDbo->setQuery($update)->execute();
    }
}
