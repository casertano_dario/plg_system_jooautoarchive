<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.jooautoarchive
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (C) 2013-2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 *
 * @version     16.5.15
 */
defined('_JEXEC') or exit;

jimport('joomla.form.formrule');

/**
 * Class JFormRuleDays
 */
class JFormRuleDays extends JFormRule
{
    protected $regex = '^([0-9]{1,4})$';
}
